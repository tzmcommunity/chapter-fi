---
layout: event

#title
title: Kesätapaaminen 2019

# after this date event wont be shown
enddate: 20190721-21:00

# banner image
header_image: "/assets/img_scaled/tzmfi_summermeeting.png"
full_image: "/assets/img/tzmfi_summermeeting.png"

# wether page and link to it is shown
hide: false

---

##### Date: 20/07/2019

##### Location: Helsinki

##### Details:


Hei kaikki,

TZM Suomi järjestää 20.7. Lauantaina live-tapaamisen Helsingissä Töölönlahdenpuistossa lähellä Keskustakirjasto Oodia. Olet tervetullut liittymään seuraamme keskustelemaan ja hengaamaan. Erityisesti haluamme kutsua kaikki Maailma Kylässä-festivaalilla meistä kiinnostuneet paikalle tutustumaan tarkemmin toimintaamme. Kokoonnumme Oodin edessä klo 15 aikaan ja siirrymme siitä, puistoon, tai jos sää niin vaatii, sisätiloihin. Liity seuraamme.

Facebook-tapahtuma [https://www.facebook.com/events/853982771667709](https://www.facebook.com/events/853982771667709/)

