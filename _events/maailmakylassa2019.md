---
layout: event

#title
title: Maailma kylässä 25.-26.5.2019

# after this date event wont be shown
enddate: 20190527-21:00

# banner image
header_image: "/assets/img_scaled/banner.png"
full_image: "/assets/scaled/banner.png"

# wether page and link to it is shown
hide: false
---


##### Date: 25/05/2019

##### Location: Helsinki

##### Details:

Tule tapaamaan meitä ja keskustelemaan Maailma kylässä-festivaaleille.
