---
layout: home
bootstrap: true

# title
postnav_title: "Zeitgeist-liike Suomi"

# second title
postnav_subtitle: "Luo kanssamme uusi yltäkylläisyyteen perustuva tulevaisuus – Resurssipohjainen talous."

# second link
postnav_link: "lisaa"

# second linktext
postnav_linktext: "Lue lisää"

# home page header image
header_image: "/assets/img/autumn-219972_1280.jpg"

---
