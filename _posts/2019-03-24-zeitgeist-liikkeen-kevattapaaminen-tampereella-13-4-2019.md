---
ID: 1302
title: Zeitgeist-liikkeen kevättapaaminen Tampereella 13.4.2019
author: Juuso Vilmunen
#excerpt: ""
layout: post
hide: false
post_date: 2019-03-24 20:25:20
---

<p>Lauantaina 13.4. järjestämme Zeitgeist-liikkeen Suomen osaston valtakunnallisen tapaamisen Tampereella. Kokoonnumme Sampolan kirjaston Ritva -salissa kello 14.00. Tapaaminen jatkuu iltaan.</p>



<p> Tapahtuma on kaikille avoin ja pitkälti vapaamuotoinen. Toivomme paikalle kaikkia liikkeen asioista kiinnostuneita juttelemaan ja viettämään aikaa yhdessä. Myöhempäänkin kellon aikaan voi tietenkin liittyä joukkoon. <br></p>



<p>PosSu ry:n kevätkokous pidetään samassa yhteydessä ennen tapaamista</p>
