---
title: Metaprojekti projekteista
author: Juuso Vilmunen
layout: post
hide: false
---

Millaisia toimia ja projekteja haluaisit Suomen osaston toteuttavan?

Millaisia haluaisit itse lähteä toteuttamaan?

Haluamme edetä liikkeemme päämääriä kohti ja koemme, että muutosta luodaan konkreettisten projektien kautta. Siksi olemme luoneet uuden Telegram-ryhmän, jossa keskustelemme millaisia projekteja liike lähtee Suomessa toteuttamaan.

Jos mielenkiintosi heräsi, liity mukaan Telegram-ryhmäämme:
<a href="https://t.me/joinchat/FEpygQ86sA_oUumfnwxrrQ">https://t.me/joinchat/FEpygQ86sA_oUumfnwxrrQ</a>