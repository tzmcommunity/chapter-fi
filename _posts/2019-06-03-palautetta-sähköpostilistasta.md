---
layout: post
title: Palautetta sähköpostilistasta
author: ''
hide: false

---
Suuri osa sähköpostilistallemme liittyneistä ei ole koskaan vahvistaneet osoitetta ja osa onkin jo ihmetelllyt tavatessa, että sähköposteja ei ole kuulunut. Näyttäisi siltä, että vahvistusviestit monessa tapauksessa merkitään roskapostiksi.

Asiaa on aika vaikea todentaa joten palautetta kaivataan!

Listalle voi liittyä täällä [https://tzm.fi/sahkopostilista/](https://tzm.fi/sahkopostilista/ "https://tzm.fi/sahkopostilista/")

Tapoja osallistua toimintaan voi katsella täältä [https://tzm.fi/toiminta/](https://tzm.fi/sahkopostilista/ "https://tzm.fi/sahkopostilista/")