---
ID: 1270
title: Maailma ilman rahaa
author: TZM Suomi
excerpt: Ihmiskunta on kriisissä kestävän kehityksen suhteen. Meillä on kestävyysvajetta niin ekologisesti, sosiaalisesti kuin ekonomisestikin. Teknologia on viemässä palkkatyön historiaan ja samalla hajottamassa instituutioita, joille koko yhteiskuntamme rakentuu. 
permalink: /maailmailmanrahaa.html
image: /uploads/MaailmaIlmanRahaa.PNG
layout: post
hide: false
post_date: 2019-05-26 15:29:24
---

## Maailma ilman rahaa on väistämätön

Ihmiskunta on kriisissä kestävän kehityksen suhteen. Meillä on kestävyysvajetta niin ekologisesti, sosiaalisesti kuin ekonomisestikin. Teknologia on viemässä palkkatyön historiaan ja samalla hajottamassa instituutioita, joille koko yhteiskuntamme rakentuu. Epätasa-arvo maailmassa kasvaa ja ekologinen kriisi uhkaa ilmastokatastrofeineen. Globaalisti ruuantuotantoa uhkaa kuivuus ja aavikoituminen. Uhkia liittyy yhteiskuntien joka osa-alueeseen. Tiivistettynä olemme pulassa. Merkittävä yhteiskunnallinen muutos lähitulevaisuudessa on väistämätön. Tästä syystä esimerkiksi jakamistalouden erilaisten muotojen ja käytäntöjen käyttö ja kannatus on yleistynyt merkittävästi viime vuosien aikana. Jopa täysin ilman rahaa ja kaupankäyntiä toimivan yhteiskunnan kannattaminen on nostanut päätään. Zeitgeist-liike on ollut mukana edesauttamassa tätä liikettä. Me näemme tällaisen maailman ei pelkästään mahdollisena ja tärkeänä, vaan väistämättömänä seuraavana askeleena ihmiskunnan historiassa. 

## Maailma ilman rahaa on toteutettavissa

Rahattoman maailman toteuttamiseen liittyy kaksi tärkeää näkökulmaa, tekninen ja sosiaalinen.
Teknisellä näkökulma tarkastelee sitä, miten hyödykkeiden tuotanto toteutetaan taloudessa, joka toimii ilman rahaa ja vaihdantaa. Sosiaalinen näkökulma taas tarkastelee sitä, millaisia sosiaalisia rakenteita yhteiskunta ilman rahaa tarvisi toimiakseen.

Tekniikka kehittyy suhteellisesti nopeammin uusien asioiden parissa. Esimerkiksi autot kehittyvät suhteellisen nopeaa tahtia 1900-luvun alussa, kun autot olivat vielä uusi ilmiö yhteiskunnassa. Mitä pidemmälle tekniikka alalla on kehittynyt, sitä vaikeampaa ja hitaampaa sitä on viedä eteenpäin.

Nykyinen tuotantotekniikka on pitkälti kehitetty sopimaan yhteiskuntaan, joka pyörii rahan avulla. Ilman rahaa toimivien yhteisöjen syntyminen ja kasvaminen alkaa luomaan uudenlaista tuotantotekniikkaa, joka taas soveltuu paremmin juuri rahattomaan yhteiskuntaan. Ja voidaan olettaa, että juuri alkuvaiheessa tämä kehitys olisi suhteellisen nopeaa, joka saattaa omalta osaltaan nopeuttaa rahattomien yhteisöjen ja yhteiskuntien yleistymisen.

Tämä ei todellakaan tarkoita, että nykyiset tuotantovälineet ja tuotantotekniikka olisi jollain tavalla hyödytöntä yhteiskunnassa, joka toimisi ilman rahaa. Ei lähimainkaan. Nykyinen tuotatotekniikka on niin kehittynyttä, että monia asioita voidaan tehdä täysin ilman ihmisen valvontaa, ja useampiakin jos halukkuutta siihen löytyisi.

Tuotantotekniikkaan liittyen tulee ymmärtää kaksi merkittävää ilmiötä, automaatio sekä ephemeralisaatio.

Ephemeralisaatiolla tarkoitetaan sitä ilmiötä, että pystymme jatkuvasti tuottamaan enemmän hyödykkeitä vähemmällä työllä ja resursseilla. Tämä ilmiö on havaittavissa koko ihmisen historian ajan. Käytännössä tämä tarkoittaa sitä, että pystymme tarjoamaan aina vain korkeamman elintason, samalla kun ihmisten työajat lyhentyisivät ja käytettyjen resurssien määrä vähentyisi. Ephemeralisaatio ilmiönä kuvaa ennen kaikkea isoja linjoja. Tämä kehityskulku on kuitenkin tärkeää ymmärtää, sillä tekniikan kehittyminen johtaa väistämättä siihen että ihmisten työnteolle on aina vain vähemmän tarvetta.

Automaatiolla tarkoitetaan jo olemassa olevan työn kehittämistä tavalla, jossa laitteet ja järjestelmät toimivat itsestään, ilman ihmisen ohjausta. Automaatio on nykyisin tärkeä tekijä ephemeralisaatioon liittyen, jopa niin tärkeä että sitä on hyvä tarkastella itsessään. Automaatiolla voidaan merkittävästi vähentää ihmisen työn tarvetta ja se on merkittävä askel eteenäin mekanisaatiosta, joka vähensi työn tarvetta tuotannossa tuomalla koneita helpottamaan työntekoa. 

Kun ymmärretään ephemeralisaation väistämättömyys ja automaation potentiaali voidaan nähdä, että tuotantoteknisesti meidän on mahdollista rakentaa talousjärjestelmä, joka vaatii toimiakseen ihmiseltä vain marginaalisen määrän työtä. Tietenkään tämä ei voi tapahtua silmänräpäyksessä, mutta jos otamme tavoitteeksemme pyrkiä mahdollisimman tehokkaasti vähentää tarvittavaa työmäärää ihmisiltä, voisimme esimerkiksi parinkymmenen vuoden päästä olla tilanteessa, jossa yhteiskuntannan ylläpito vaatii työntekoa vain muutamalta prosentilta yhteiskunnan jäsenistä, tai toisinpäin käännettynä vain pari tuntia viikossa kaikilta. 

Jos yhteiskunta ei tarvitse suurta työllisyyttä toimiakseen, tarvitsemme uudenlaisia sosiaalisia rakenteita yhteiskuntaamme. Näitä tarvitaan myös organisoimaan työtä, jota vähäisyydestään huolimatta silti tarvitaan talouden pyörittämiseen. Niihin liittyy rahattoman maailman toteuttamisen sosiaalinen näkökulma.

Sosiaaliset rakenteet ovat koko ihmislajin menestyksen perusta. Ihminen pystyy organisoitumaan monella eri tavalla sekä sopeutumaan mitä erilaisimpiin sosiaalisiin rakenteisiin. Itse asiassa juuri raha on todella hyvä esimerkki tästä kyvystä. Rahalla ei itsessään ole juurikaan itseisarvoa, mutta yhteiskunnan kollektiivinen luottaminen sen arvoon mahdollistaa kyseisen instituution menestyksen. Kieltämättä raha on ollut merkittävä tekijä ihmiskunnan kehityksessä. Tämä ei kuitenkaan tarkoita, etteikö kaupankäynnillä ja rahaan perustuvalla yhteiskunnalla olisi haittapuolia, joita pystyttäisiin poistamaan rahattomalla yhteiskunnalla.

Merkittävin yksittäinen sosiaalinen rakenne, joka vaatisi muutosta rahattomaan yhteiskuntaa siirtyessä on osakeyhtiö. Yrityksen ovat todella merkittävässä roolissa, yhteiskunnassamme talouden pyörittämisen, sekä tutkimus- ja kehitystyön näkökulmasta. Nykyiseltään yritysten sisäinen rakenne on todella voimakkaasti rakennettu palkkatyön varaan. Edes johtoasemissa olevat henkilöt eivät todennäköisesti jatkaisi työtänsä, jos heille ei siitä maksettaisi. Tässä ei ole kyse mistään ihmisluonnon pohjimmaisesta laiskuudesta, se voidaan jo nähdä siinä työnmäärästä, jota ihmiset ovat valmiita tekemään palkatta toisenlaisten sosiaalisten rakenteiden parissa, kuten esimerkiksi perheen, vapaaehtoisjärjestön taikka omaksi koetun yhteisön parissa. 

Juuri yhteisöllisyys on todella merkittävä tekijä ihmisen työskentely motivaatiossa. Ihmisen ovat valmiita tekemään työtä yhteisön eteen, johon he itse kokevat kuuluvansa. Yhteisöllisyys ei tarvitse välttämättä edes konkreettisia ihmissuhteita. Hyvä esimerkki yhteisöllisyydestä on verkkosivusto Redditin aprillipäivänä toteuttama sosiaalinen koe “Place”, jossa käyttäjät saivat värittää tietyin aikavälein yhden pikselin suuresta kuvasta. Erilaiset yhteisöt taistelivat pitääkseen haluamansa logon tai tunnuksen näkyvissä lopputuloksessa. Ihmiset olivat valmiita, jopa uhraamaan yöunensa kantaakseen kortensa kekoon oman yhteisönsä eduksi. Ja lopputuloksella ei ollut minkäänlaista muuta arvoa, kuin sosiaalista. Ihmiset työnteollaan pystyivät takaamaan vain sen, että heidän yhteisönsä tuli nähdyksi.

Luomalla sopivia sosiaalisia rakenteita, pystymme takaamaan, sen että ihmiset todella mielellään tekevät työtä, jolla he pitävät yhteiskunnan pystyssä. Kestävissä olosuhteissa ihmiset kokevat, että heidän työllään on paljon merkitystä, ja että he kantavat kortensa kekoon oman yhteisönsä hyvinvoinnin eteen. Tällöin he tekevät sitä mielellään, vaikka heille ei siitä palkkaa maksettaisiin. Tietenkin tällaisten rakenteiden luomisessa auttaa valtavasti se, ettei työtä välttämättä tarvitse tehdä 40 tuntia viikossa, vaan ennemminkin 40 tuntia kuukaudessa tai jopa aikanaan 40 tuntia vuodessa.


## Maailma ilman rahaa on parempi

 Ihminen joutuu elämään koko elämänsä ristiriidassa omien luonnollisten tarpeidensa ja ulkopuolelta asetettujen vaikutusten kanssa. Myötätunto ja halu auttaa muita ihmisiä sekä muita elämänmuotoja peittyy helposti mainonnan, pr-toiminnan, keskinäisen kilpailun sekä toimimattomien yhteiskuntarakenteiden aikaansaamien ajatustottumusten ja keinotekoisten tarpeiden alle.

 Rahaton maailma poistaa yhteiskunnasta valtavasti negatiivisia sivuvaikutuksia ja ihmisten välisiä jännitteitä.
 
 Ilman rahaa ei olisi tarvetta kilpailulle jolloin kaikki hyötyisivät muiden menestyksestä. Tällaisessa maailmassa hyödykkeitä ei tuotettaisi myytäväksi ja tuottoa tavoitellen, vaan ylläpitämään sekä parantamaan elämänlaatua. Ei olisi myöskään tarvetta kalliille mainoskampanjoille jolloin yhteiskunnallinen kysyntä alkaisi heijastelemaan ihmisten todellisia tarpeita ja haluja. Tiivistettynä, talous ja yhteiskunta sen mukana alkaisi pyöriä ihmiskeskeisesti, sillä talouden toimijoita ei enää motivoisi ainoastaan tuotto.

 Jos kaikki perustarpeet täytetään kaikille ihmisille ilman mitään velvoitteita, on kaikilla mahdollisuus herätä elämään elämäänsä myötätuntoisesti muita ja luontoa kohtaan.


## Maailma ilman rahaa on mahdollinen
 Vain ymmärryksen kautta on mahdollista toimia paremman yhteiskunnan mahdollistamiseksi. On kuitenkin tärkeä muistaa, että ei ole lopullista ymmärryksen tasoa. On siis aina enemmän opittavaa. Uuden oppimisen kautta voit rakentaa uusia näkökulmia ja pystyt hahmottamaan yhteiskunnan toimintaa kokonaisvaltaisemmin. 

 Opi muilta ja jaa ymmärrystäsi muille. Voit vaikkapa luettuasi tämän lehtisen, jakaa sen eteenpäin ystävällesi. Suosittelemme tutustumaan tarkemmin TZM:n materiaaleihin. 
