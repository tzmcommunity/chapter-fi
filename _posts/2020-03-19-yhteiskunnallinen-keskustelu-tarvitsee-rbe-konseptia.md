---
layout: post
title: Miksi yhteiskunnallinen keskustelu kaipaa resurssipohjaisen talouden konseptia?
author: zeitgeist -liike
hide: false
permalink: /artikkelit/yhteiskunnallinen-keskustelu-tarvitsee-rbe-konseptia/
thumbnail: "uploads/fbimg20200326.png"
image: uploads/fbimg20200326.png

post_date: 2020-03-19 22:00:00 +0000

---

**Resurssipohjainen talous** tarjoaa uuden näkökulman vallitsevaan yhteiskunnalliseen keskusteluun. Parhaimmillaan kyseinen konsepti voisi ohjata yhteiskunnan kehittymistä kohti ihmisten ja luonnon hyvinvointia sekä kestävää uudenlaista talousjärjestelmää. Resurssipohjaisen talouden konseptista tekee merkittävän se, että sen ajattelu yhdistää kestävän kehityksen sekä kestävyyteen vaadittavan arvomaailman muutoksen samaan kokonaisvaltaiseen näkökulmaan.

<!--more-->

Resurssipohjainen talous, **Resource-Based Economy (RBE)** on alunperin kaupunkisuunnittelija Jacque Frescon luoma termi. Fresco tarkoitti resurssipohjaisella taloudella yhteiskuntajärjestelmää, joka hyödyntää uusimpia teknologioita ja tiedettä mahdollisimman korkean elintason tarjoamiseen kaikille maailman ihmisille. Frescolle ydinasia oli, että kaikki maapallon resurssit julistetaan kaikkien maan asukkaiden yhteiseksi perinnöksi. Resurssit, hyödykkeet ja palvelut olisivat kaikkien saatavilla ilman tarvetta minkäänlaiselle vaihtokaupalle. Frescon ajatus perustui sille, että hyödyntämällä sekä jakamalla järkevästi ja kestävästi maapallon resursseja, teknologiaa avuksi käyttäen, voisimme luoda yltäkylläisen yhteiskunnan, jossa tarve ihmisten työpanokselle on murto-osa nykyisestä. 

Keskeinen ilmiö tämän potentiaalin ymmärtämiseen on tuottavuuden jatkuva kasvu, jota Buckminster Fuller kutsui nimellä **ephemeralisaatio**. Teknologian kehittyminen johtaa siihen, että pystymme tuottamaan koko ajan enemmän samalla käyttäen vähemmän resursseja. Tämä kehitys johtaa vääjäämättä tilanteeseen, jossa pystymme marginaalisella työn määrällä tuottamaan tarvittavat asiat, tuotteet ja palvelut koko ihmiskunnan ylläpitämiseen. Resurssipohjainen talous tarjoaa näkökulman, jossa automaatio ja muu teknologinen kehittyminen ei aja ihmisiä työttömyteen ja köyhyyteen, vaan vapauttaa ihmiset työnkuvista, joita he eivät tekisi, jos siitä ei heille maksettaisi. 

Ephemeralisaatio takaa pitkällä aikavälillä tehokkaan resurssien hyödyntämisen, mutta lyhyellä aikavälillä jakamistalouden edistäminen mahdollistaa nopean kasvun tehokkuudessa, jolla resursseja hyödynnämme. **Jakamistalous** pystyy mahdollistamaan saman elintason merkittävästi vähemmillä resursseilla, kuin talous, jossa jokainen ihminen omistaa itse käyttämänsä hyödykkeet. Tarve omistaa on taakka yksilöiden henkilökohtaiselle taloudelle, sekä resurssien käytön myötä tälle planeetalle. Onneksi jakamistalouden noususta on merkkejä muun muassa yhteiskäyttöautojen tai työkalulainaamojen muodossa.

Resurssipohjaiseen talouteen liittyy yhtäläisyyksiä Marxin näkemykseen luokattomasta yhteiskunnasta eli kommunismista. RBE:hen liittyy myös hyvin paljon liberalismin arvoja. Ajatus resurssipohjaisesta taloudesta eroaa lähtökohdiltaan kuitenkin 1800-luvun suurista aatteista. Nämä aatteet pohjautuvat pitkälti siihen, minkälainen tulisi oikeudenmukaisen yhteiskunnan olla. RBE keskittyy kuitenkin täysin siihen, millainen olisi mahdollisimman tehokas ja hyvinvoiva yhteiskunta, eli toimiva yhteiskunta. Puhuessamme tehokkuudesta yhteiskunnassa, tarkoitamme sitä, millä työn ja resurssien määrällä yhteiskunta pystyy ylläpitämään mahdollisimman korkeaa kansanterveyden tasoa. Tarpeeksi tehokas yhteiskunta pystyy kestävästi ylläpitämään korkeaa kansanterveyden tasoa, toisin sanoen yhteiskunta on hyvinvoiva.

Keskeistä resurssipohjaisessa taloudessa on **tekninen lähestymistapa** yhteiskunnallisiin ongelmiin. Yhteiskunnallisiin ongelmiin tulisi suhtautua kuten teknisiin ongelmiin – pyritään etsimään paras mahdollinen ratkaisu. Jos ongelma ei poistu tai vähene merkittävästi, kyseessä ei ole ratkaisu. Jos ratkaisu ei ole toteuttamiskelpoinen, kyse ei ole mahdollisesta ratkaisusta. Esimerkiksi ruoantuotantoa suunnitellessa tulisi löytää keino tuottaa tarpeeksi ravintoa yhteisölle siten, että käytetään mahdollisimman tehokkaasti vaadittavia resursseja ja pidetään huolta siitä, että toiminta on kestävällä pohjalla. Ratkaisuksi ei silloin kelpaisi malli, jota yhteisö ei onnistu ylläpitämään ihmisten työskentelyyn liittyvien epärealististen arvioiden vuoksi.

Resurssipohjaisen talouden ajattelu tuo esille mitä kaikkea olisi mahdollista saavuttaa, jos ihmiskunta työskentelisi yhdessä kohti kestävää ja toimivaa yhteiskuntaa. Mikä kaikki olisikaan mahdollista, jos emme käyttäisi kallisarvoisia luonnonvaroja taikka arvokkaita ihmiselämän tunteja esimerkiksi sotakalustojen valmistukseen, heikkolaatuisiin kuluttajatuotteisiin tai halpatyöllä tuotettuun pikamuotiin. Ilman tarvetta oman toimeentulon turvaamiselle, jäisi paljon valitettavia asioita tekemättä. Ilman tarvetta yritysten kilpailukyvyn turvaamiselle, voitaisiin keskittyä mahdollisimman pitkäikäisten tuotteiden valmistukseen sekä materiaalien optimaaliseen uusiokäyttöön ja kierrätykseen.

Resurssipohjaisen talouden tai samankaltaisten konseptien eteen tekee työtä nykyään todella moni taho. Varmasti suurinta osaa näistä järjestöistä yhdistää se, että niille ei ole niin tärkeää miksi konseptia kutsutaan. Kunhan päämääränä on yleinen hyvinvointi kestävällä pohjalla, ei ole merkitystä kutsutaanko sitä resurssipohjaiseksi taloudeksi vaiko, joksikin aivan muuksi. Resurssipohjaisen talouden nimen kautta on työskennellyt kaksi merkittävää tahoa, The Venus Project ja The Zeitgeist Movement. 

**Venus Project** on resurssipohjaisen talouden konseptin luojan Jacque Frescon ja Roxanne Meadowsin perustama järjestö, joka tähtää toteuttamaan resurssipohjaista taloutta perustamalla tutkimuskeskuksen ja lopulta kokeellisen kaupunkiyhteisön, jonka toiminta perustuisi resurssipohjaiseen talouteen. Venus Project on jo aloittanut tutkimuskeskuksensa suunnittelemisen ja rahoittamisen. Lisätietoa Venus Projektista ja sen toiminnasta löydät osoitteesta: https://www.thevenusproject.com 

**The Zeitgeist Movement, TZM**, sai alkunsa Peter Josephin Zeitgeist-elokuvatrilogian toisesta osasta, Zeitgeist Addendum. TZM toimi alunperin Venus Projectin aktivistihaarakkeena, mutta erkaantui vuonna 2011 omaksi liikkeekseen. TZM toimii globaalisti edistääkseen resurssipohjaisen talouden ajattelua yhteiskunnassa.

Resussipohjainen talous on yhteiskuntajärjestelmänä niin erilainen nykyiseen yhteiskuntarakenteeseen verrattuna, ettei muutos siihen voi tapahtua kovin nopeasti. Fresco itse ajatteli, että todellisen RBEn saavuttaminen saattaisi viedä satoja vuosia. Se voidaan kuitenkin saavuttaa askel askeleelta, mutta vain jos se otetaan päämääräksi. Ihminen on erittäin sopeutuvainen eläin, ja se tulee varmasti sopeutumaan myös yhteistyöhön ja yltäkylläisyyteen perustuvaan yhteiskuntaan.

**Holistinen näkökulma** ihmiskunnan toimintaan tuo esille sen, että olemme kaikki samassa veneessä tällä planeetalla. Emme ole toistemme vihollisia tai kilpakumppaneita, olemme joukkuetovereita. Olemme kaikki osa ihmiskuntaa, jolle haluamme rakentaa mahdollisimman hyvän yhteiskunnan. Yhteistyöllä pystymme luomaan kestävällä pohjalla olevan yltäkylläisyyden kaikille maailman ihmisille. Ihmiskunnan yhteiselo ei ole nollasummapeliä. Tällä hetkellä vaikuttaa siltä, että yhteiskuntamme kaipaa kipeästi muistutusta tästä. Frescon yksi tunnetuimmista sitaateista tarjoaa hyvän lopetuksen tälle tekstille; **“The smarter your kids are, the richer my life will be.”**

