---
layout: post
title: PERUTTU - Zeitgeist Kevätristeily Ruotsiin
author: zeitgeist -liike
hide: false
thumbnail: "uploads/2020cruise.png"

post_date: 2020-03-04 22:00:00 +0000

---

## Risteily on peruttu

Harmillista kyllä, mutta epidemian takia tapahtuma on peruttu. Katselemme uutta ajankohtaa kunhan virustilanne selkiytyy.

## Tapahtuman tiedot

Monta vuotta siitä on puhuttu, mutta nyt se vihdoin toteutuu. Zeitgeist-liikkeen Suomi-Ruotsi risteily.

Kansainvälisen yhteistyö merkeissä lähdemme tervehtimään naapureitamme Tuhkolmaan.

Sieltä paikalliset aktiivit jatkavat kanssamme Helsinkiin.

Kaikki kiinnostuneet ovat tervetulleita mukaan risteilylle. Risteily on: To 16.04.2020 klo 17.15, M/S Gabriella

Hytin voi varata Viking Linen sivuilta: [https://www.vikingline.fi/](https://www.vikingline.fi/ "https://www.vikingline.fi/").

Hyttiseuraa voit kysellä tämän tapahtuman keskustelussa, niin saadaan yhdistettyä hyttejä ja risteiltyä tehokkaammin.

Pidämme ensin torstaina 16.4. Post-scarcity Support ry:n kevätkokouksen Oodissa.

Sen jälkeen luvassa on risteily Zeitgeist-liikkeen Ruotsin osaston kanssa.

Lauantaina voi liittyä Helsingissä mukaan, mikäli risteilylle ei ole mahdollista osallistua.

Facebook tapahtuma: [https://www.facebook.com/events/184178226343679/](https://www.facebook.com/events/184178226343679/ "https://www.facebook.com/events/184178226343679/")

<!--more-->

### Aikataulu

#### Yhdistyksen evätkokous

To 16.4. klo 15.00 Post-scarcity support Ry:n kevätkokous Helsingissä
Keskustakirjasto Oodi, Ryhmätila 5 (2. krs.)

#### Menomatka M/S Gabriella

To 16.04.2020 klo 17.15 - Lähtö Helsingistä
Pe 17.04.2020 klo 10.00 - Saapuminen Tukholmaan

#### Päivä Tukholmassa

Tukholmassa tapaamme Ruotsin osaston väkeä, kiertelemme kaupungilla ja suuntaamme sitten yhdessä takaisin satamaan paluumatkalle.

#### Paluumatka M/S Gabriella

Pe 17.04.2020 klo 16.30 - Lähtö Tukholmasta
La 18.04.2020 klo 10.10 - Saapuminen Helsinkiin

#### Lauantaina päivä Helsingissä

La 18.04.2020 klo 12.00 - Kaikki innokkaat mukaan tapaamaan.
La 18.04.2020 klo 17.15 - Ruotsista kyytiin tulleiden lähtö Helsingistä