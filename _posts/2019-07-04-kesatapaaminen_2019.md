---
title: Kesätapaaminen 2019
layout: post
author: me
hide: false
post_date: 2019-07-04 17:00:00

---

Hei kaikki,

TZM Suomi järjestää 20.7. Lauantaina live-tapaamisen Helsingissä Töölönlahdenpuistossa lähellä Keskustakirjasto Oodia. Olet tervetullut liittymään seuraamme keskustelemaan ja hengaamaan. Erityisesti haluamme kutsua kaikki Maailma Kylässä-festivaalilla meistä kiinnostuneet paikalle tutustumaan tarkemmin toimintaamme. Kokoonnumme Oodin edessä klo 15 aikaan ja siirrymme siitä, puistoon, tai jos sää niin vaatii, sisätiloihin. Liity seuraamme.

Facebook-tapahtuma [https://www.facebook.com/events/853982771667709](https://www.facebook.com/events/853982771667709/)
