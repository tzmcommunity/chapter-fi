---
layout: post
title: Tapoja olla yhteydessä
author: 'zeitgeistliike'
hide: false
post_date: 2019-07-14 17:00:00
#permalink: /uutiskirjeet/201907.html
excerpt: >
  Tässäpä siis lista tapoja jolla seurata toimintaamme ja olla yhteydessä<br>
  
 
---

Pelkkä Facebookin käyttö on ongelmallista kaltaisellemme liikkeelle. Nykyään facebookin ongelmat esimerkiksi yksityisyyden suhteen tunnetaan hyvin. Haluammekin kannustaa ihmisiä käyttämään muita alustoja.

Tässäpä siis lista tapoja jolla seurata toimintaamme ja olla yhteydessä.

- Sähköpostiosoitteemme: info@tzm.fi
- Webbi-sivustomme: [https://tzm.fi](https://tzm.fi) 
- Sähköpostilistallemme voi liittyä vaikka täällä [https://tzm.fi/toiminta/](https://tzm.fi/toiminta/)
- Telegram kanavalla voit seurata uutisia: [https://t.me/tzmsuomi](https://t.me/tzmsuomi)
- Tervetuloa keskustelemaan “TZM Keskustelu” -ryhmään: [https://t.me/joinchat/HJFMZlMBV9brkQwWAVCpZA](https://t.me/joinchat/HJFMZlMBV9brkQwWAVCpZA)
- Suomenkielinen sivu Minds sivustolla [http://bit.ly/tzm-fi-minds](http://bit.ly/tzm-fi-minds) ja ryhmä [http://bit.ly/tzm-fi-minds-group](http://bit.ly/tzm-fi-minds-group)
- Matrix yhteisö: (Esim Riot.im käyttäjille) [https://matrix.to/#/+tzm:matrix.tzm.community](https://matrix.to/#/+tzm:matrix.tzm.community)
- Kansainvälisen Discord palvelimme josta löytyy myös suomen kielinen kanava [https://discord.gg/CnnjAKc](https://discord.gg/CnnjAKc)
- tzm.community news sivusto joka päivittyy discord -palvelimelle tehtyjen julkaisujen mukaan: [https://news.tzm.community/](https://news.tzm.community/)
