---
layout: post
title: Kesän 2019 kuulumiset Zeitgeist liikkeeltä
author: 'zeitgeistliike'
hide: false
post_date: 2019-07-14 17:00:00
permalink: /uutiskirjeet/201907.html
tags:
  - uutiskirje
excerpt: >
  Uusimmat kuulumiset liikkeeltä (koko uutiskirje klikkaamalla otsikkoa)<br>
  <br />
  - Live tapaaminen 20.7. <br />
  - Telegram kanavat ja ryhmä <br />
  - uusi webbi sivusto <br />
  - Discord palvelin <br />
  - Maailma kylässä -festivaalit <br />
  - Yhdistyksen kevätkokous <br />
 
---


## Kesätapaaminen 20.7. Helsingissä
TZM Suomi järjestää 20.7. Lauantaina live-tapaamisen Helsingissä Töölönlahdenpuistossa lähellä Keskustakirjasto Oodia. Olet tervetullut liittymään seuraamme keskustelemaan ja hengaamaan. Erityisesti haluamme kutsua kaikki Maailma Kylässä-festivaalilla meistä kiinnostuneet paikalle tutustumaan tarkemmin toimintaamme. Kokoonnumme Oodin edessä klo 15 aikaan ja siirrymme siitä puistoon, tai jos sää niin vaatii, sisätiloihin. Liity seuraamme.

## Pysy yhteydessä uusilla alustoilla
Telegram kanavalla voit seurata uutisia: [https://t.me/tzmsuomi](https://t.me/tzmsuomi)

Tervetuloa keskustelemaan "TZM Keskustelu" -ryhmään: [https://t.me/joinchat/HJFMZlMBV9brkQwWAVCpZA](https://t.me/joinchat/HJFMZlMBV9brkQwWAVCpZA)

Osaston sivu Minds sivustolla [https://www.minds.com/tzmsuomi](https://www.minds.com/tzmsuomi)

tzm.community news sivusto joka päivittyy discord -palvelimelle tehtyjen julkaisujen mukaan: [https://news.tzm.community/](https://news.tzm.community/)

## Webbi-sivut
Sivustot (tzm.fi ja zeitgeist.fi) päivitettiin käyttämään liikkeen eri osastojen käyttöön tarkoitettua tzm.community alustaa. Sivustot luodaan lähdekoodista automaattisesti tehokkaiksi staattisiksi sivuiksi. Ulkoasussa on vielä viilattavaa, mutta hyötyö tulee koko globaalille liikkeele kun samat muutokset ulkoasussa voidaan ottaa käyttöön myös muiden osastojen sivustoilla.

## Discord-palvelin
Käyttäjämäärä globaalilla Discord-palvelimellamme on hyvässä nousussa. Tervetuloa sinnekin keskustelemaan. Tässä kutsulinkki [http://bit.ly/tzm-discord](http://bit.ly/tzm-discord).

## Maailma kylässä -festivaalit
Osallistuimme toista kertaa Maailma kylässä -festivaaleille. Porukkaa oli mukavasti liikkeellä vaikka sää ei suosinutkaan. Paljon tavattiin uusia ihmisiä ja käytiin mielenkiintoisia keskusteluita!

## Yhdistyksen kevätkokous
Huhtikuussa pidimme liikeeen kevät tapaamisen ja Post-scarcity Support Ry:n kevätkokouksen Tampereella. Kokouksen pöytäkirja löytyy [täältä](https://drive.google.com/file/d/1A180hLOcWb_of6anqCNMW-nBBRBaXnQk/view?usp=sharing)

