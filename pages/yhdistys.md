---
ID: 1249
title: Post-scarcity Support Ry
author: Juuso Vilmunen
#excerpt: ""
layout: page
permalink: /yhdistys/
parentid: suomenosasto
hide: false
post_date: 2019-02-03 14:57:29
---

Tästä linkistä <a href="https://drive.google.com/open?id=1lqCBzFBHJIhv0tNQ__xUsy5mUiAmp6a2">yhdistyksen säännöt</a>.



Avoin budjetti: <a href="https://holvi.com/avoinbudjetti/tzm-fi/">https://holvi.com/avoinbudjetti/tzm-fi/</a>



<a href="https://docs.google.com/spreadsheets/d/11-52xrid8P6CnBXLQXVsX1R9FLOWCS1J4l_q-z_tYRw/edit?usp=sharing">Tulot ja menot-taulukko</a><a href="https://docs.google.com/spreadsheets/d/11-52xrid8P6CnBXLQXVsX1R9FLOWCS1J4l_q-z_tYRw/edit?usp=sharing">-taulukko</a>


<!-- wp:html -->
<iframe width="100%" height="700" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSTHkHzO2Ykl-T95CKVsW8StczpXXV3bNjyKkSh2PAD_CR-o6L-Lb0oE7yGnPybygK2tbnITNGi91wU/pubhtml?widget=true&amp;headers=false"></iframe>
<!-- /wp:html -->



