---
layout: page
title: News
tags: [News, Archive]
order: 3
hide: true
---

{% include base.html %}

{% for post in site.posts %}

   ## 
     <a href="{{ base }}{{ post.url }}">
       {{ post.title }}
     </a>
   
   <time datetime="{{ post.date | date: "%Y-%m-%d" }}">{{ post.date | date_to_long_string }}</time>
   {{ post.content | strip_html | truncate: "150"}}

{% endfor %}
