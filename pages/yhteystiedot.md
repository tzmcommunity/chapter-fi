---
ID: 16
title: Yhteystiedot
author: root
#excerpt: ""
layout: page
permalink: /yhteystiedot/
parentid: lisaa
hide: false
post_date: 2014-05-12 04:31:59
---
<p style="text-align: left;"><span style="font-size: 16pt;">Voit käyttää yhteydenottoon alla olevaa kaavaketta, tai sähköpostiosoitettamme:
zeitgeistfinland­[ät]gmail.com</span>

<form action="https://formspree.io/zeitgeistfinland@gmail.com" method="POST" >
  <div class="form-group">
    <label for="name">Nimi</label>
    <input type="name" class="form-control" name="name" aria-describedby="emailHelp" placeholder="Your name" />
    <small id="nameHelp" class="form-text text-muted"></small>
  </div>
  <div class="form-group">
    <label for="email">Sähköpostiosoite</label>
    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email" />
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="title">Aihe</label>
    <input type="title" class="form-control" name="title" aria-describedby="emailHelp" placeholder="Aihe" />
    <small id="titleHelp" class="form-text text-muted"></small>
  </div>

  <div class="form-group">
    <label for="title">Teksti</label>
    <textarea class="form-control" name="text" aria-describedby="tekstiHelp" placeholder="" rows="10" ></textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


<p style="text-align: left;"><span style="font-size: 16pt;">Löydät meidät myös <span style="text-decoration: underline;"><a href="https://www.facebook.com/pages/Zeitgeist-liike-Suomi/415241325286402">facebookista</a></span> ja <span style="text-decoration: underline;"><a href="https://discord.gg/vpVcd56">discordista</a></span></span>
