---
ID: 437
title: Uutiskirjeet
#excerpt: ""
layout: page
permalink: /uutiskirjeet/
parentid: suomenosasto
hide: false
post_date: 2014-12-02 17:37:36
---

{% for post in site.posts %}
{% for tag in post.tags %}

{% if "uutiskirje" == tag %}
  
   <h3> <a href="{{ post.url }}">
       {{ post.title }}
     </a>
   </h3>
   
   <time datetime="{{ post.date | date: "%Y-%m-%d" }}">{{ post.date | date_to_long_string }}</time>
   {{ post.content | strip_html | truncate: "150"}}

{% endif %}
{% endfor %}
{% endfor %}
