---
ID: 437
title: Osallistu
author: Teemu Koskimäki
#excerpt: ""
layout: page
permalink: /osallistu/
parentid: suomenosasto
hide: false
post_date: 2014-12-02 17:37:36
---
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSciXSSCWWCXGWIACHJe2TmJF8kVQf6Dy8hP32tuUZZHghShjA/viewform?embedded=true" width="780" height="500" frameborder="0" marginwidth="0" marginheight="0">Loading...</iframe>
<em>Eikö pelitä? Voit täyttää alla olevan kyselyn myös täällä: <a href="https://goo.gl/forms/SB79XTeDaAPlEbMH3" target="_blank">https://goo.gl/forms/SB79XTeDaAPlEbMH3</a></em>