---
ID: 486
title: Z-liike videoina
author: Teemu Koskimäki
#excerpt: ""
layout: page
permalink: /z-liike-videoina/
parentid: opi
hide: false
post_date: 2015-02-12 00:01:07
---




[divider type="dotted" spacing="8"]



[well]<strong>Kuinka voit osallistua:</strong>

Lähetä liikkeen ajatusmaailman kannalta hyödyllinen videolinkki (mielellään YouTube) <strong>JA</strong> sen mukana lyhyt selitys seuraavista: <strong>1.</strong> Mitä videossa käsitellään? Ja <strong>2.</strong> Miksi koet sen olevan oleellinen Z-liikkeen kannalta?

Linkin saateteksteineen voit lähettää etusivun lomakkeen kautta, tai suoraan sähköpostitse osoitteeseen zeitgeistfinland [ät] gmail.com

Voit myös julkaista ne <a href="https://www.facebook.com/ZeitgeistSuomi">Fb-sivullamme</a>. Laita otsikoksi projektin nimi, esim. “Z-LIIKE VIDEOINA”.[/well]