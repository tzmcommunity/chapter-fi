---
ID: 698
title: Toiminta
author: Teemu Koskimäki
#excerpt: ""
layout: page
permalink: /toiminta/
parentid: suomenosasto
hide: false
post_date: 2015-11-29 21:47:48
---

## <a href="https://tzm.community/discord">TZM Suomi Discordissa</a>
Helpointa TZM Suomen kanssa keskustelu on Discordissa! Pyrimme pitämään Discordin aina auki, jotta kanavallemme voi tulla aina juttelemaan TZM aiheista samanmielisten ihmisten kanssa. Keskustella voi tekstichattina (aula) ja/tai äänellä (keskustelukanava) oman valintansa.

<a href="/2018/04/28/kuukausittaiset-avoimet-tapaamiset-vakioaikana.html">Kuukausittaiset avoimet tapaamiset</a> pidetään myös Discordissa.

<iframe src="https://discordapp.com/widget?id=333971123211599874&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>


## <a href="http://www.zeitgeist.fi/sahkopostilista/">Liity sähköpostilistalle</a>
Haluamme, että kaikki liikkeemme kannattajat pysyisivät jollain tasolla perillä siitä, mitä liikkeessämme tapahtuu ja mitä toiminnallemme kuuluu tällä hetkellä. Sekä kansallisella, että kansainvälisellä tasolla. Tarkoituksenamme on lähettää tälle sähköpostilistalle neljä kertaa vuodessa katsaus, jossa annetaan tilannepäivitystä liikkeen toiminnasta ja tapahtumista.

Liity siis tälle listalle, jos olet kiinnostunut Zeitgeist-liikkeen toiminnasta. Ota huomioon, että sähköpostiosoite vahvistetaan ja vahvistusviesti saattaa löytyä roskapostikansiosta.

<script type="text/javascript" src="https://s3.amazonaws.com/phplist/phplist-subscribe-0.2.min.js"></script>

<script type="text/javascript">var thanksForSubscribing = '<div class="subscribed">Thanks for subscribing. Please check your inbox and click the link in the request for confirmation message.'; </script>

<p id="phplistsubscriberesult">
<form action="https://zeitgeistliike.hosted.phplist.com/lists/?p=subscribe&id=2" method="post" id="phplistsubscribeform">
<input type="text" name="email" value="" id="emailaddress" />
<button type="" id="phplistsubscribe">Subscribe to our newsletters</button>
</form>



## <a href="/tue-toimintaa/">Tue toimintaa</a>

Voit tukea toimintaamme myös liittymällä yhdistyksemme jäseneksi tai lahjoitammalla.




## <a href="http://www.zeitgeist.fi/osallistu/">Osallistu -kysely</a>
Valitse kiinnostuksen kohteesi ja jätä sähköpostiosoitteesi tässä <a href="http://www.zeitgeist.fi/osallistu/">kyselyssä</a>, niin ilmoitamme sinulle kun tapaamisissamme käsitellään sinua kiinnostavia aiheita! Ilmoitukset saa peruttua jälkikäteen.


## <a href="https://trello.com/b/Wguxxrpl/joukkoistettu-tehtavalista-zeitgeist-liike-suomi">Joukkoistettu aktivismiympäristö</a>
Aktivismiympäristöön kuuluu kolme osaa:
<ul>
 	<li><a class="known-service-link" href="https://trello.com/b/7EPfDk1K/pysyvat-tehtavat"><img class="known-service-icon" src="https://d78fikflryjgj.cloudfront.net/images/services/e1b7406bd79656fdd26ca46dc8963bee/trello.png" />  <strong>Pysyvät tehtävät</strong></a>
<ul>
 	<li><em><em>Listaus tehtävistä joita ei ole tarkoitus saada "valmiiksi". Pysyviksi listattuihin tehtäviin voi osallistua kuka vaan ja ne ovat jaoteltu vaikeusasteen mukaan. Esim: "Z-LIIKE KUVINA" tai "Haaste: Tiivistä liikkeen ideologia yhteen lauseeseen!"</em></em></li>
</ul>
</li>
 	<li><a class="known-service-link" href="https://trello.com/b/uZgnyIj6/liikkuvat-tehtavat"><img class="known-service-icon" src="https://d78fikflryjgj.cloudfront.net/images/services/e1b7406bd79656fdd26ca46dc8963bee/trello.png" />  <strong>Liikkuvat tehtävät</strong></a>
<ul>
 	<li><em><em>Kehittyviä tehtäviä, jotka voidaan jaotella sen mukaan kuinka lähellä valmistumista ne ovat. Liikkuvat tehtävät on tarkoitus saada valmiiksi ja poistettua taululta. Esim: "Käytännön opas katuaktivismiin." tai "RBE-tietokannan/-oppimisympäristön rakentaminen".</em></em></li>
</ul>
</li>
 	<li><a class="known-service-link" href="https://trello.com/b/bAI6gmfw/lahivuosien-suunnitelma"><img class="known-service-icon" src="https://d78fikflryjgj.cloudfront.net/images/services/e1b7406bd79656fdd26ca46dc8963bee/trello.png" />  <strong>Lähivuosien suunnitelma</strong></a>
<ul>
 	<li><em>Tämän avulla voimme seurata liikkeen suunnitelmien kehittymistä sekä määritellä vastuuhenkilöitä ja määräaikoja suunnitelmien toteuttamista varten. Tehtävien suorittamisen tarkoitus on levittää tietoisuutta ja saavuttaa lähivuosien suunnitelmaan listatut pitkän aikavälin tavoitteemme.
</em></li>
</ul>
</li>
</ul>

## <a href="https://trello.com/paikallisosastot">Paikallisosastot</a>
Käytämme Trelloa paikallisosastojen listaamiseen sitä mukaa kun niitä syntyy. Voit tarkistaa kiinnostuneiden määrän alueellasi tzm.fi etusivun kartalta!


## Rationaalinen konsensus -ryhmä
RK (Rationaalinen konsensus) -ryhmä koostuu ihmisistä jotka panostavat omaa aikaansa liikkeen koordinointiin ja toiminnan suunnitteluun sekä toteuttaamiseen. Ryhmä koordinoi Zeitgeist-liikettä Suomessa.</span>

Ryhmässä mm. jaamme liikkeen moderoinnin ja koordinoinnin kannalta oleellista tietoa, suunnittelemme tapaamisia ja keskustelemme niihin liittyvistä asioista. RK-ryhmän jäsenet ymmärtävät liikkeen ajatuksia, toimintatapoja ja periaatteita.

Kaikki kommunikatiomme ja päätöksemme perustuvat rationaaliseen konsensukseen: <a href="http://on.fb.me/1q8uLiD" target="_blank" rel="nofollow noopener" data-lynx-uri="http://l.facebook.com/?u=http%3A%2F%2Fon.fb.me%2F1q8uLiD&amp;e=ATMq2nL1wS2FwTC_TEbMmmePxiFMouXX_95csMa95bGFiWgbYqGPCfhMVwnog8LckCTZuEVd6Cy6FY7y49g33mktUVLocMlgEoXlG8MbkQm0QMFksRx3ZIjONo6hD0IYTseD6Q8">http://on.fb.me/1q8uLiD</a>

Ota yhteyttä, jos koet ymmärtäväsi liikkeen periaatteita ja olet kiinnostunut liikkeen koordinoinnista sekä kehittämisestä!



<strong>Tervetuloa mukaan toimintaan!</strong>



