---
ID: 125
title: Ajan henki -dokumenttielokuva
author: Mikko
layout: page
permalink: "/ajan-henki/"
parentid: lisaa
hide: false
post_date: '2014-06-22T22:01:01.000+00:00'
tags: []
order: 
toc: false
menuid: ''

---
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/5WyGgvvqLok" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</center>

Ajan henki on kotimainen voittoa tavoittelematon dokumenttielokuva, jonka tarkoitus on tuoda esiin yhteiskuntamme ongelmien perimmäiset syyt sekä tarjota vaihtoehto vallitsevalle ajan hengelle. Tämä dokumenttielokuva toteutettiin talkoovoimin "nollabudjetilla" ja tarjotaan ilmaislevityksenä koko kansakunnalle!
<h3>Ajan henki muualla</h3>
<ul>
 	<li><a href="http://www.facebook.com/group.php?gid=133766763337092&amp;ref=mf">Facebook</a></li>
 	<li><a href="magnet:?xt=urn:btih:1D62AE035BFE7B2215433FE6114506573B011364&amp;dn=Ajan%20Henki%202011%20720p%20AAC3.1%20H264&amp;tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&amp;tr=udp%3a%2f%2ftracker.publicbt.com%3a80%2fannounce&amp;tr=udp%3a%2f%2ftracker.ccc.de%3a80%2fannounce">Torrent</a></li>
</ul>