---
ID: 540
title: Artikkelit
author: Mikko
#excerpt: ""
layout: page
permalink: /artikkelit/
hide: false
post_date: 2015-03-01 18:46:07
---

{% for post in site.posts %}
{% if post.hide==false %}

## [{{ post.title }}]({{ post.url }})
   
   <time datetime="{{ post.date | date: "%Y-%m-%d" }}">{{ post.date | date_to_long_string }}</time>
   <p>
   {{ post.excerpt | strip_html }}
   </p>
   
{% endif %}   
{% endfor %}
