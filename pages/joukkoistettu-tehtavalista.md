---
ID: 638
title: Joukkoistettu tehtävälista
author: Teemu Koskimäki
#excerpt: ""
layout: page
order: 1
permalink: /joukkoistettu-tehtavalista/
parentid: suomenosasto
hide: false
post_date: 2015-09-18 15:05:32
---
<p style="text-align: left;"><span style="font-family: RBNo2Light; font-size: 24pt;">Katso ensin ohjeet tästä kortista:</span>
<a href="https://trello.com/c/T2fF2Gre" target="_blank"><img class=" alignleft" src="https://trello.com/c/T2fF2Gre.png" alt="" /></a>
<em>Tutki, kommentoi, osallistu.</em>
<iframe src="https://trello.com/b/Wguxxrpl.html" width="100%" height="800" frameborder="0"></iframe>
<span style="font-size: 18pt;"><em>"Meidän ei tarvitse odottaa sitä mitä muut tekevät."</em> </span>
<span style="font-size: 18pt;">― Mahatma Gandhi</span>
<span style="font-family: RBNo2Light; font-size: 24pt;"><a href="https://trello.com/b/Wguxxrpl/joukkoistettu-tehtavalista-zeitgeist-liike-suomi">»</a><a href="https://trello.com/b/Wguxxrpl/joukkoistettu-tehtavalista-zeitgeist-liike-suomi">Koko lista«</a></span>