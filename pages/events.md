---
layout: page
title: Events
tags: [Events, Archive]
order: 4
hide: true
---

Events placeholder

{% include base.html %}

{% for event in site.events %}

   ## 
     <a href="{{ base }}{{ event.url }}">
       {{ event.title }}
     </a>
   
   {{ event.excerpt }}

{% endfor %}
