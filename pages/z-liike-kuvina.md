---
ID: 550
title: Z-liike kuvina
author: jufda
#excerpt: ""
layout: page
permalink: /z-liike-kuvina/
parentid: opi
hide: false
post_date: 2015-03-01 19:45:06
---


<a href="http://www.zeitgeist.fi/z-liike-kuvina/"><button type="button">Zeitgeit-liike kuvina</button></a> <a href="http://www.zeitgeist.fi/toiminta2/julisteiden-jako-projekti-2016/"><button type="button">Julisteiden jako -projekti</button></a>




[gallery type="rectangular" ids="731,727,730,726,732,735,728,729,733,734,1166,1165,1164,1163"]



[divider type="dotted" spacing="10"]

<strong>Kuinka voit osallistua:</strong>

Lähetä meille kuva, joka mielestäsi kuvailee hyvin Z-liikettä tai jotain sen käsittelemistä aihealueista. Hienoa olisi, jos teet kuvan itse. Kerro lähettäessäsi kuvan lähde. Voit lähettää kuvaehdotukset s-postilla osoitteeseen zeitgeistfinland [ät] gmail .com. Voit myös julkaista ne <a href="https://www.facebook.com/ZeitgeistSuomi">Fb-sivullamme</a>. Laita otsikoksi projektin nimi, esim. “Z-LIIKE KUVINA”.
<ul>
 	<li>Haemme:
<ul>
 	<li>Meemi tyyppisiä kuvia, mielellään suomenkielisiä.</li>
 	<li>Z-aiheista taidetta (maapallokuvia, logoja, postereita), yms.</li>
</ul>
</li>
</ul>


Kuvien kautta voi oppia ja ymmärtää nopeasti. Niitä voi jakaa omatoimisessa nettiaktivissa ja niitä voi myös esimerkiksi tulostaa ja levitellä kaupungilla, kouluissa ja niin edelleen.



<em><strong>PS.</strong> Suomen osasto tarvitsee siistejä grafiikoita joita voimme käyttää nettisivuja rakentaessamme, joten sellaiset ovat erittäin tervetulleita.</em>
<ul>
 	<li><em>Tärkeä osa aktivismia on näyttää mahdollisimman ammattimaiselta. Tähän liittyy kiinteästi tarve siistille ja yksinkertaiselle ulkoasulle. Tässä sinä voit auttaa. Tee/muokkaa liikkeen aiheisiin liittyviä kuvia ja lähetä ne meille!</em></li>
</ul>
