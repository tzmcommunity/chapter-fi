---
ID: 1171
title: Sähköpostilista
author: Juuso Vilmunen
#excerpt: ""
layout: page
permalink: /sahkopostilista/
parentid: suomenosasto
hide: false
post_date: 2018-08-13 09:26:12
---
Haluamme, että kaikki liikkeemme kannattajat pysyisivät jollain tasolla perillä siitä, mitä liikkeessämme tapahtuu ja mitä toiminnallemme kuuluu tällä hetkellä. Sekä kansallisella, että kansainvälisellä tasolla. Tarkoituksenamme on lähettää tälle sähköpostilistalle neljä kertaa vuodessa katsaus, jossa annetaan tilannepäivitystä liikkeen toiminnasta ja tapahtumista.

Liity siis tälle listalle, jos olet kiinnostunut Zeitgeist-liikkeen toiminnasta. Ota huomioon, että sähköpostiosoite vahvistetaan ja vahvistusviesti saattaa löytyä roskapostikansiosta.

<script type="text/javascript" src="https://s3.amazonaws.com/phplist/phplist-subscribe-0.2.min.js"></script>

<script type="text/javascript">var thanksForSubscribing = '<div class="subscribed">Thanks for subscribing. Please check your inbox and click the link in the request for confirmation message.'; </script>

<p id="phplistsubscriberesult">
<form action="https://zeitgeistliike.hosted.phplist.com/lists/?p=subscribe&id=2" method="post" id="phplistsubscribeform">
<input type="text" name="email" value="" id="emailaddress" />
<button type="" id="phplistsubscribe">Subscribe to our newsletters</button>
</form>
